package main.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import main.Exceptions.*;
import main.Operation;
import main.SaisieRPN;;


public class SaisieRPNTest {
	
	private SaisieRPN saisie;

	@Before
	public void setUp(String str) throws exception{
		saisie = new SaisieRPN(str);
	}
	
	
	@Test
	public void getOperationtest() {
		assertEquals(Operation.PLUS,saisie.getOperation("+"));
		assertEquals(Operation.MOINS,saisie.getOperation("-"));
		assertEquals(Operation.DIV,saisie.getOperation("/"));
		assertEquals(Operation.MULT,saisie.getOperation("*"));
	}
	
	@Test
	public void estOperationTest() {
		assertEquals(true,saisie.estOperation("+"));
		assertEquals(true,saisie.estOperation("-"));
		assertEquals(true,saisie.estOperation("/"));
		assertEquals(true,saisie.estOperation("*"));
	}
	
}
