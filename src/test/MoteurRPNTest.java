package main.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.Exceptions.*;
import main.MoteurRPN;
import main.Operation;


public class MoteurRPNTest {
	private MoteurRPN moteurrpn;
	
	@Before
	public void setUp() {
		  moteurrpn = new MoteurRPN();
	}
 
	
	@Test
    public void EmpilerTest() throws exception {
		moteurrpn.empiler("2");
		moteurrpn.empiler("3");
		assertEquals((Double)3.0,(Double)moteurrpn.getpile_val().peek()); 
    }
	
	@Test
	public void estOperandtest() {
		assertEquals(true,moteurrpn.estOperand("15.0"));
	}
	
	@Test(expected = minoperandException.class)
    public void EmpilerUneValeurMoinsDeMinValueTest() throws exception {
		moteurrpn.empiler("-9999");    
    }
	
	@Test(expected = maxoperandException.class)
    public void EmpilerUneValeurPlusDeMaxValueTest() throws exception {
		moteurrpn.empiler("9999");   
    }
	
	@Test
    public void calculerTest() throws exception {
		moteurrpn.empiler("6");
		moteurrpn.empiler("2");
		assertEquals((Double)7.0,Operation.PLUS.eval(6.0, 2.0));
		assertEquals((Double)4.0,Operation.MOINS.eval(6.0, 2.0));
		assertEquals((Double)3.0,Operation.DIV.eval(6.0, 2.0));
		assertEquals((Double)12.0,Operation.MULT.eval(6.0, 2.0));
    }
	
	@Test(expected = pilevideException.class)
    public void PileVideTest() throws exception {
		moteurrpn.depiler();   
    }
	
	 
	 @Test(expected = exception.class)
	 public void CalculerAvecUnSeulOperandTest() throws exception {
		 moteurrpn.empiler("2.0");
		 moteurrpn.calculer("+");
	  }
	 
	 
	  @Test
		public void CalculerAvecPlusieursOperandTest() throws exception {
		 moteurrpn.empiler("2.0");
		 moteurrpn.empiler("3.0");
		 moteurrpn.empiler("4.0");
		 moteurrpn.empiler("-2.0");
		 moteurrpn.calculer("+");
		 moteurrpn.calculer("+");
		 moteurrpn.calculer("+");
		 assertEquals((Double)7.0,null);
	  }
	  
	  @After
		public void ResultatOperandTest() throws exception {
		 moteurrpn.empiler("5.0");
		 moteurrpn.empiler("-2.0");
		 moteurrpn.empiler("4.0");
		 moteurrpn.empiler("1.0");
		 moteurrpn.calculer("+");
		 moteurrpn.calculer("+");
		 moteurrpn.calculer("+");
		 assertEquals((Double)8.0,(Double)moteurrpn.getpile_val().peek());
	  }
}