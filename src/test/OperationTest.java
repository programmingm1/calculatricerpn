package main.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import main.Exceptions.*;
import main.Operation;

public class OperationTest {
	private Double Operande1;
	private Double Operande2;
	
	@Before
	public void setUp() {
		Operande1 = new Double(15);
		Operande2 = new Double(3);
	}
 
	@Test
    public void AdditionTest() throws exception{
		 assertEquals((Double)(Operande2 + Operande1),(Double)Operation.PLUS.eval(Operande2 , Operande1));
     }
	
	@Test
    public void SoustractionTest() throws exception{
		assertEquals((Double)(Operande2 - Operande1),(Double)Operation.MOINS.eval(Operande2 , Operande1));
     }
	
	@Test
    public void DivisionTest() throws exception{
       assertEquals((Double)(Operande2 / Operande1),(Double)Operation.DIV.eval(Operande2 , Operande1));
     }
	
	@Test
    public void MultiplicationTest() throws exception{
		assertEquals((Double)(Operande2 * Operande1),(Double)Operation.MULT.eval(Operande2 , Operande1));
     }
	

	@Test(expected = divisionException.class)
    public void divisionparZero() throws exception {
		Operation.DIV.eval(Operande2, 0.0);
       
    }
}
