package main;

import java.util.*;
import main.Exceptions.*;

public enum Operation {
	
	PLUS("+") {
        public Double eval(Double Operande1, Double Operande2) throws exception {
            return Operande2 + Operande1;
        }
    },

    MOINS("-") {
        public Double eval(Double Operande1, Double Operande2) throws exception {
            return Operande2 - Operande1;
        }
    },

    MULT("*") {
        public Double eval(Double Operande1, Double Operande2) throws exception {
            return Operande2 * Operande1;
        }
    },

    DIV("/") {
        public Double eval(Double Operande1, Double Operande2) throws exception {
            if (Operande1 == 0)
            	throw new divisionException();
            return Operande2 / Operande1;
        }
    };


    private String symbole;

    Operation(String symbole) {
        this.symbole = symbole;
    }

    public abstract Double eval(Double Operande1, Double Operande2) throws exception;
    
    public String getSymbole() {
        return symbole;
    }
    
    public static Operation getOperateur(String op) throws exception{
    	switch (op) {
          case "+": return PLUS;
          case "-": return MOINS;
          case "*": return MULT;
          case "/": return DIV;
          default: throw new operateurException();
    	}
        	
       }
}	
