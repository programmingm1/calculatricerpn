package main;

import java.util.Stack;
import main.Exceptions.*;

public class MoteurRPN {

    private static Stack<Double> pile_val = new Stack<Double>();
    Double MAX_VALUE =100.0;
    Double MIN_VALUE =-100.0;
    
    /**
     * cr�ation de la pile pile_val
     * @throws exception
     */
    public MoteurRPN() 
	{
    	
	}
    
    public String estOperand(String str) {
		
		try {
			Double.parseDouble(str);
		} catch (NumberFormatException e){
			return null;
		}
 
		return str;
	}
	
    
    /**
     * empiler les op�randes
     * @param input 
     * @throws exception
     */
    public void empiler(String value) throws exception {
    	
        if (estOperand(value) == null)
        	//faire l'operation
        {
            this.calculer(value);
            System.out.println("Elements de la pile: " + pile_val);
        }
         else {
            // c'est un nombre r�el
        	 if(Double.parseDouble(value)<MIN_VALUE)
        	 {
        		 System.out.println("min valeur: " + MIN_VALUE);
        	 
        		 throw new minoperandException();}
        	 else {
        		 if(Double.parseDouble(value)>MAX_VALUE)
        		 {
            		 System.out.println("min valeur: " + MIN_VALUE);
            		 
       		         throw new maxoperandException();}
                 else
            	{
            		pile_val.push(Double.parseDouble(value));
            		System.out.println("Elements de la pile: " + pile_val);
            	}
        	      }
              }

        }

    /**
     * verifier nature de la chaine entr�e
     * @param str 
     * @throws NumberFormatException
     */
    
    
    
    public Double depiler()
    {
    	return pile_val.pop();
    }
    
    /**
     * Executer une op�ration dans la pile
     * @param operatorString op�rateur valide
     * @throws exception
     */
    public void calculer(String OperateurString) throws exception {

        // pile vide ou non
        if (pile_val.isEmpty()) {
            throw new pilevideException();
        }

        // recherche operateur
        Operation operateur = Operation.getOperateur(OperateurString);
        if (operateur == null) {
        	throw new operateurException();
        }


        // depiler les op�randes

        if(pile_val.isEmpty())
        	throw new exception("pile vide (pas d'op�randes)");
        Double operande1 = this.depiler();
        
        if(pile_val.isEmpty())
        	throw new exception("pile vide (pas d'op�randes)");
        Double operande2 = this.depiler();
        
        // calculer resultat
        Double result = operateur.eval(operande1, operande2);
        
        // empliler resultat
        if (result != null) 
        	pile_val.push(result);
        /*}catch (exception e)
        {
        	throw new exception("pile vide (pas d'op�randes)");
        }*/


    }


    /**
     * Returner la valeur du pile_val
     */
    public Stack<Double> getpile_val() {
        return pile_val;
    }

    /**
     * Returner tableau des valeurs de la pile
     */
    public Double[] affichepile(){
    	
    	Double[] t=new Double [this.pile_val.size()];
    	int i=0;
    	while (!pile_val.isEmpty())
    	{
    		t[i]=pile_val.pop();
    		i++;
    	}
    	
    	for (i=this.pile_val.size()-1;i>=0;i--)
    		pile_val.push(t[i]);
    	return t;	
    }


}